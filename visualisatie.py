import pygame

class Visualisatie:
    def __init__(self, spel, fps=20):
        pygame.init()
        self.spel = spel
        self.linksbovenx = 10
        self.linksboveny = 10
        self.hoklengte = 55
        self.muurdikte = 15
        self.muurkleur = (255, 255, 0)
        self.pion_1_kleur = (0, 0, 255)
        self.pion_2_kleur = (255, 0, 0)
        self.semimuurdikte = int(self.muurdikte/2)
        self.pionradius = int((self.hoklengte - self.muurdikte)/2.1)
        self.schermbreedte = 2*self.linksbovenx + 9*self.hoklengte
        self.schermhoogte = 2*self.linksboveny + 9*self.hoklengte

        self.logo = pygame.image.load("logo.png")
        pygame.display.set_icon(self.logo)
        pygame.display.set_caption("interactief_scherm")
        self.interactief_scherm = pygame.display.set_mode((self.schermbreedte,
                                                           self.schermhoogte))
        self.timer = pygame.time.Clock()
        self.fps = fps

    def visualisatieupdate(self):
        self.timer.tick(self.fps)
        
        def beginpunt_muur(self, orientatie, y, x):
            if orientatie == "horizontaal":
                beginpunt = [self.linksbovenx + self.hoklengte*x + self.semimuurdikte + 1,
                             self.linksboveny + self.hoklengte*(y+1)]
            else:
                beginpunt = [self.linksbovenx + self.hoklengte*(x+1),
                             self.linksboveny + self.hoklengte*y + self.semimuurdikte + 1]
            return(beginpunt)

        def eindpunt_muur(self, orientatie, y, x):
            if orientatie == "horizontaal":
                eindpunt = [self.linksbovenx + self.hoklengte*(x+2)
                            - self.semimuurdikte - 1,
                            self.linksboveny + self.hoklengte*(y+1)]
            else:
                eindpunt = [self.linksbovenx + self.hoklengte*(x+1),
                            self.linksboveny + self.hoklengte*(y+2)
                            - self.semimuurdikte - 1]
            return(eindpunt)

        self.interactief_scherm.fill((0,0,0))
        
        for orientatie, y, x in ((orientatie, y, x) for orientatie in self.spel.muren
                                 for y in range(self.spel.aantal_muren)
                                 for x in range(self.spel.aantal_muren)):
            if self.spel.muren[orientatie][y][x] == 1:
                pygame.draw.line(self.interactief_scherm, self.muurkleur,
                                 beginpunt_muur(self, orientatie, y, x),
                                 eindpunt_muur(self, orientatie, y, x), self.muurdikte)

        pygame.draw.rect(self.interactief_scherm, self.muurkleur,
                         (self.linksbovenx, self.linksboveny, 9*self.hoklengte,
                          9*self.hoklengte), self.muurdikte)

        pygame.draw.circle(self.interactief_scherm, self.pion_1_kleur,
                           (int(self.linksbovenx + self.hoklengte*(self.spel.pion_1.x+1/2)),
                            int(self.linksboveny + self.hoklengte*(self.spel.pion_1.y+1/2)) ),
                           self.pionradius, self.pionradius)
        pygame.draw.circle(self.interactief_scherm, self.pion_2_kleur,
                           (int(self.linksbovenx + self.hoklengte*(self.spel.pion_2.x+1/2)),
                            int(self.linksboveny + self.hoklengte*(self.spel.pion_2.y+1/2)) ),
                           self.pionradius, self.pionradius)

        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                return False

    def check_sluiten(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                print("Het spel wordt gesloten")
                return False

    def sluit_scherm(self):
        pygame.quit()
        

