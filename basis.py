from random import choice
import pygame
from visualisatie import Visualisatie

class Richting:
    def __init__(self, naam, dx, dy, muur_1_dx, muur_1_dy, muur_2_dx, muur_2_dy, index):
        self.naam = naam
        self.dx = dx
        self.dy = dy
        if self.dx == 0:
            self.blokkerende_muur_orientatie = "horizontaal"
            # Als de richting van een zet de x-coordinaat onveranderd laat, dan zijn het
            # horizontale self.muren die de zet kunnen blokkeren
        else:
            self.blokkerende_muur_orientatie = "verticaal"
        self.muur_1_dx = muur_1_dx
        self.muur_1_dy = muur_1_dy
        self.muur_2_dx = muur_2_dx
        self.muur_2_dy = muur_2_dy
        self.index = index

    def draaiboek(self, richtingen):
        return [richtingen[(self.index - 1) % 4], self,
                richtingen[(self.index + 1) % 4], richtingen[(self.index + 2) % 4]]

class Pion:
    muren_per_speler = 10  # Pas naar wens aan
    def __init__(self, index):  # Neem 1 of 2 als index
        self.index = index
        self.beschikbare_muren = self.muren_per_speler
        self.x = 4
        if self.index == 1:
        # De pion met index 1 staat onder en de pion met index 2 staat boven
            self.y = 8
            self.eindstreep = 0
        else:
            self.y = 0
            self.eindstreep = 8

    def __repr__(self):
        return f"pion {self.index}, met als eindstreep {self.eindstreep}"

class Spel:
    noord = Richting("noord", 0, -1, 0, -1, -1, -1, 0)
    oost = Richting("noord", 1, 0, 0, 0, 0, -1, 1)
    zuid = Richting("zuid", 0, 1, 0, 0, -1, 0, 2)
    west = Richting("west", -1, 0, -1, 0, -1, -1, 3)
                # ("naam", dx, dy, muur_1_dx, muur_1_dy, muur_2_dx, muur_2_dy, index)
    richtingen = [noord, oost, zuid, west]
    
    def __init__(self):
        self.aantal_hokjes = 9  # Pas naar wens aan
        self.aantal_muren = self.aantal_hokjes - 1
        self.muren = {"horizontaal": [[0] * self.aantal_muren
                                      for _ in range(self.aantal_muren)],
                      "verticaal": [[0] * self.aantal_muren
                      for _ in range(self.aantal_muren)]}
        # self.muren["horizontaal"/"verticaal"][y][x] geeft een 0 als er geen muur staat
        # en een 1 als er wel een muur staat
        self.pion_1 = Pion(1)
        self.pion_2 = Pion(2)
        self.aan_de_beurt = self.pion_1
        self.pionnen = [self.pion_1, self.pion_2]

    def stap_geen_muur_wel_vak(self, x, y, richting):
        """ Kijkt of er geen muur is die verhinderd
        dat een pion op (x, y) in richting kan lopen,
        en kijkt of de pion dan niet van het bord af loopt. """
        return (0 <= x + richting.dx <= self.aantal_muren and
                0 <= y + richting.dy <= self.aantal_muren and
                ((not 0 <= x + richting.muur_1_dx < self.aantal_muren) or
                 (not 0 <= y + richting.muur_1_dy < self.aantal_muren) or
                 self.muren[richting.blokkerende_muur_orientatie]
                           [y + richting.muur_1_dy][x + richting.muur_1_dx] == 0) and
                ((not 0 <= x + richting.muur_2_dx <= self.aantal_muren) or
                 (not 0 <= y + richting.muur_2_dy <= self.aantal_muren) or
                 self.muren[richting.blokkerende_muur_orientatie]
                           [y + richting.muur_2_dy][x + richting.muur_2_dx] == 0))

    def mogelijke_pionzetten(self, pion):
        """ Geeft een lijst van de mogelijke plekken
        waarheen de pion zich mag verplaatsen. """
        zetten = []
        for richting in Spel.richtingen:
            if self.stap_geen_muur_wel_vak(pion.x, pion.y, richting):
                # Als er geen muur in de weg staat
                if (pion.x + richting.dx == self.pion_1.x and
                    pion.y + richting.dy == self.pion_1.y or
                    pion.x + richting.dx == self.pion_2.x and
                    pion.y + richting.dy == self.pion_2.y):
                    # Als er een pion staat op de nieuwe plaats
                    if self.stap_geen_muur_wel_vak(pion.x + richting.dx,
                                       pion.y + richting.dy, richting):
                        # Als er geen muur in de weg staat
                        zetten.append((pion.x + 2*richting.dx,
                                       pion.y + 2*richting.dy))
                    elif self.stap_geen_muur_wel_vak(pion.x + richting.dx,
                                        pion.y + richting.dy,
                                        self.richtingen[(richting.index + 1) % 4]):
                        zetten.append((pion.x + richting.dx
                                       + self.richtingen[(richting.index + 1) % 4].dx,
                                       pion.y + richting.dy
                                       + self.richtingen[(richting.index + 1) % 4].dy))                    
                    elif self.stap_geen_muur_wel_vak(pion.x + richting.dx, pion.y + richting.dy,
                                         self.richtingen[(richting.index + 3) % 4]):
                        zetten.append((pion.x + richting.dx
                                       + self.richtingen[(richting.index + 3) % 4].dx,
                                       pion.y + richting.dy
                                       + self.richtingen[(richting.index + 3) % 4].dy))
                else:
                    # Als er geen pion staat
                    zetten.append((pion.x + richting.dx, pion.y + richting.dy))
        return zetten

    def geen_muur_overlap(self, orientatie, y, x):
        """ Kijkt of een muur op de plaats (orientatie, y, x) neergezet kan worden,
        dus zodat die muur niet overlapt of kruist met een al geplaatsde muur. """
        if self.muren[orientatie][y][x] == 0:
            if orientatie == "horizontaal":
                if self.muren["verticaal"][y][x] == 1:
                    return 0
                if x == 0 or self.muren[orientatie][y][x-1] == 0:
                    if (x == self.aantal_hokjes - 2 or self.muren[orientatie][y][x+1] == 0):
                        return 1
            else:
                if self.muren["horizontaal"][y][x] == 1:
                    return 0
                if y == 0 or self.muren[orientatie][y-1][x] == 0:
                    if (y == self.aantal_hokjes - 2 or self.muren[orientatie][y+1][x] == 0):
                        return 1
        else:
            return 0

    def mogelijke_muurzetten(self, pion):
        """ Geeft een lijst met beschikbare plekken voor een muur
        in formaat (orientatie, y, x) afhankelijk van je pion. """
        zetten = []
        if pion.beschikbare_muren > 0:
            for orientatie in self.muren:
                for y in range(self.aantal_muren):
                    for x in range(self.aantal_muren):
                        if self.geen_muur_overlap(orientatie, y, x) == 1:
                            zetten.append((orientatie, y, x))
        return zetten

    def mogelijke_zetten(self, pion):
        """ Geeft een lijst van de mogelijke zetten die een pion/speler kan doen.
        (Er wordt nog niet gecheckt of een route naar de overkant mogelijk is.) """
        return self.mogelijke_pionzetten(pion) + self.mogelijke_muurzetten(pion)

    def doe_zet(self, pion, zet):
        """ Voert een zet uit voor een bepaalde pion. """
        if len(zet) == 2:
            # Verplaatst de pion
            pion.x = zet[0]
            pion.y = zet[1]    
        else:
            # Plaatst een muur
            orientatie, y, x = zet
            self.muren[orientatie][y][x] = 1
            pion.beschikbare_muren -= 1

    def geef_winnaar(self):
        """ Geeft een 1 als er op dat moment een pion aan de overkant staat. """
        for pion in self.pionnen:
            if pion.y == pion.eindstreep:
                return pion
        return None

    def print_winnaar(self, beurt):
        print(f"{self.geef_winnaar()} heeft gewonnen in beurt {beurt}")

    def print_gelijk_spel(self):
        print("de pionnen zijn noobs")

    def random_spel_per_ronde(self, visualisatie=None):
        """ Speelt een quoridorspel met random zetten totdat één speler wint. """
        rondelimiet = 500  # Pas naar wens aan
        for ronde in range(rondelimiet):
            for pion in self.pionnen:
                zetten = self.mogelijke_zetten(pion)
                while True:
                    zet = choice(zetten)
                    self.doe_zet(pion, zet)
                    if len(zet) == 3:
                        if self.check_mogelijke_route():
                            break
                        else:
                            orientatie, y, x = zet
                            self.muren[orientatie][y][x] = 0
                            pion.beschikbare_muren += 1
                            print(f"het is voorkomen dat de muur {orientatie, y, x} "
                                 f"is geplaatst omdat dit de route zou blokkeren")
                    else:
                        break
                if visualisatie:
                    visualisatie.visualisatieupdate()
                if self.geef_winnaar():
                    return pion, ronde
        return False

##    def vraag_input(self):
##        """ Vraagt input van de mens, en als de mens een pion wil bewegen,
##        dan wordt pion 1 bewogen. """
##        a = True
##        while a:
##            speler_input = input("welke zet wil je doen? (muur/pion) ")
##            if speler_input == "pion" or speler_input == "muur":
##                if speler_input == "pion":
##                    input_richting = input("welke kant wil je op? (noord/oost/zuid/west) ")
##                    for richting in Spel.richtingen:
##                        if richting.naam == input_richting:
##                            self.beweeg_naar(richting)
##                            return
##                if speler_input == "muur":
##                    orientatie = input("welke orientatie moet de muur hebben? (horizontaal/verticaal) ")
##                    coordinaten = input("welke coordinaten moet de muur hebben? (x, y) ")
##                    a = False

    def andere_pion(self, pion):
        """ Dit is geen behulpzame comment. """
        if pion == self.pion_1:
            return self.pion_2
        else:
            return self.pion_1

    def een_random_beurt(self):
        """ Speelt een random quoridor beurt."""
        zetten = self.mogelijke_zetten(self.aan_de_beurt)
        self.doe_zet(self.aan_de_beurt, choice(zetten))
        self.aan_de_beurt = self.andere_pion(self.aan_de_beurt)

    def een_functie_beurt(self, functie):
        zetten = self.mogelijke_zetten(self.aan_de_beurt)
        self.doe_zet(self.aan_de_beurt, functie(zetten))
        self.aan_de_beurt = self.andere_pion(self.aan_de_beurt)

    def loop_om_muur_heen(self, x, y, vooruit_richting, eindstreep):
        orginele_positie = ()
        beste_y_coordinaat = y
        beste_x_coordinaat = x
        richting = self.richtingen[(vooruit_richting.index + 2) % 4]
        while True:
            for richting in richting.draaiboek(self.richtingen):
                if self.stap_geen_muur_wel_vak(x, y, richting):
                    x, y = x + richting.dx, y + richting.dy
                    if orginele_positie == (x, y):
                        if y == beste_y_coordinaat:
                            return False
                        return (beste_x_coordinaat, beste_y_coordinaat)
                    if abs(y-eindstreep) < abs(eindstreep-beste_y_coordinaat):
                        if y == eindstreep:
                            return True
                        beste_y_coordinaat = y
                        beste_x_coordinaat = x
                    if not orginele_positie:
                        orginele_positie = (x, y)
                    break
            else:
                return False

    def check_mogelijke_route(self):
        for pion in self.pionnen:
            x, y, index = pion.x, pion.y, pion.index
            vooruit_richting = self.richtingen[2 * (index - 1)]
            for _ in range(8):
                
                while True:
                    if self.stap_geen_muur_wel_vak(x, y, vooruit_richting):
                        y += vooruit_richting.dy
                    else:
                        break
                if y == pion.eindstreep:
                    break
                
                positie = self.loop_om_muur_heen(x, y, vooruit_richting, pion.eindstreep)
                if positie == False:
                    return False
                elif positie == True:
                    break
                else:
                    x, y = positie
                    
        return True
            
if __name__ == "__main__":
##    import timeit
##    print(timeit.timeit("""spel = Spel()
##spel.random_spel_per_ronde(visualisatie=None)""",
##                        number=100, setup="""from __main__ import Spel, Visualisatie"""))
    spel = Spel()
    visualisatie = Visualisatie(spel)
    uitslag = spel.random_spel_per_ronde(visualisatie=visualisatie)
    if uitslag:
        pion, beurt = uitslag
        print(f"{pion} heeft gewonnen in beurt {beurt}")
    else:
        print("de pionnen zijn noobs")


