import pygame
import time

from basis import *
from visualisatie import *

def random_spel(spel=None, sluit_scherm=False, fps=2):
    """ Laat een random spel zien. """
    if not spel:
        spel = Spel()
    visualisator = Visualisatie(spel, fps)
    visualisator.visualisatieupdate()
    for beurt in range(500):
        spel.een_random_beurt()
        if visualisator.visualisatieupdate() == False:
            return
        pion = spel.geef_winnaar()
        if pion:
            spel.print_winnaar(beurt)
            break
    else:
        spel.print_gelijk_spel()
    if sluit_scherm:
        visualisator.sluit_scherm()
    else:
        while True:
            if visualisator.check_sluiten() == False:
                break


def random_spel_met_veel_sprongen():
    """ Laat een random spel zien waarbij de twee pionnen opgesloten zitten
    in een 2x2 hok """
    spel = Spel()
    spel.aantal_hokjes = 9
    spel.muren = {"horizontaal": [[0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0]],
                  "verticaal":   [[0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0],
                                  [0, 0, 0, 0, 0, 0, 0, 0]]}
    spel.pion_1.x = 3
    spel.pion_1.y = 3
    spel.pion_1.beschikbare_muren = 0
    spel.pion_2.x = 4
    spel.pion_2.y = 4
    spel.pion_2.beschikbare_muren = 0

    random_spel(spel)

if __name__ == "__main__":
    random_spel(sluit_scherm=True, fps=2)
    random_spel_met_veel_sprongen()

          
